const express = require("express")
const bodyParser = require("body-parser")
const config = require("config")
const request = require("request")
const axios = require("axios")
const R = require("ramda")
const Papa = require("papaparse")

const app = express()

app.use(bodyParser.json({limit: "10mb"}))

app.get("/investments/export", async (req, res) => {

  let investments
  let companies

  try {
    investments = await axios.get(`${config.investmentsServiceUrl}/investments`).then(res => res.data)
    companies = await axios.get(`${config.financialCompaniesServiceUrl}/companies`).then(res => res.data)
  } catch (e) {
    console.error(e)
    return res.send(500)
  }

  const generateCSV = (investment) => R.map((holding) => {
    return {
      "User": holding.id,
      "First Name": investment.firstName,
      "Last Name": investment.lastName,
      "Date": investment.date,
      "Holding": R.find(R.propEq("id", holding.id))(companies).name,
      "Value": investment.investmentTotal * holding.investmentPercentage,
    }
  }, investment.holdings)

  try {
    await axios.post(`${config.investmentsServiceUrl}/investments/export`, {
      data: JSON.stringify(Papa.unparse(R.flatten(R.map(generateCSV, investments)))),
    }).then(() => {
      res.send(200)
    })
  } catch (e) {
    console.error(e)
    res.send(500)
  }
})

app.get("/investments/:id", (req, res) => {
  const {id} = req.params
  request.get(`${config.investmentsServiceUrl}/investments/${id}`, (e, r, investments) => {
    if (e) {
      console.error(e)
      res.send(500)
    } else {
      res.send(investments)
    }
  })
})

app.listen(config.port, (err) => {
  if (err) {
    console.error("Error occurred starting the server", err)
    process.exit(1)
  }
  console.log(`Server running on port ${config.port}`)
})
